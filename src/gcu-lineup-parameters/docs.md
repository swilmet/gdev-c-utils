gcu-lineup-parameters
=====================

Line up parameters of function declarations.

Example:

```
gboolean
frobnitz (Frobnitz *frobnitz,
          gint magic_number,
          GError **error)
{
  ...
}
```

Becomes:

```
gboolean
frobnitz (Frobnitz  *frobnitz,
          gint       magic_number,
          GError   **error)
{
  ...
}
```

Read the top of `gcu-lineup-parameters.c` for more details.
