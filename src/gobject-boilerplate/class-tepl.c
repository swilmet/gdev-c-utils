#include "filename.h"
#include <tepl/tepl.h>

struct _NamespaceClassnamePrivate
{
	gint something;
};

G_DEFINE_TYPE_WITH_PRIVATE (NamespaceClassname, namespace_classname, G_TYPE_OBJECT)

static void
namespace_classname_finalize (GObject *object)
{
	tepl_object_counters_decrement ("NamespaceClassname");

	G_OBJECT_CLASS (namespace_classname_parent_class)->finalize (object);
}

static void
namespace_classname_class_init (NamespaceClassnameClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = namespace_classname_finalize;
}

static void
namespace_classname_init (NamespaceClassname *self)
{
	tepl_object_counters_increment ("NamespaceClassname");

	self->priv = namespace_classname_get_instance_private (self);
}
