#!/bin/sh

# configuration

namespace_camel='Gcsv'
#namespace_camel='Gtex'

classname_camel='App'

filename='gcsv-app'

# generate the new class

template_filename='class-tepl'

./generate-class-common.sh \
	"${namespace_camel}" \
	"${classname_camel}" \
	"${filename}" \
	"${template_filename}"
