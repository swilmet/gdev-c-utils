#!/bin/sh

# Configuration

#namespace_camel='GtkSource'
#namespace_camel='Gedit'
#namespace_camel='Amtk'
namespace_camel='Tepl'
#namespace_camel='Gcsv'
#namespace_camel='Gfls'
#namespace_camel='Gtex'

classname_camel='AbstractFactory'

filename='tepl-abstract-factory'

# Generate the new class

template_filename='class'

./generate-class-common.sh \
	"${namespace_camel}" \
	"${classname_camel}" \
	"${filename}" \
	"${template_filename}"
