#!/bin/sh

# Configuration

#namespace_camel='Gedit'
namespace_camel='Gfls'
#namespace_camel='Gspell'
#namespace_camel='GtkSource'
#namespace_camel='Tepl'

interfacename_camel='LoaderConfig'

filename='gfls-loader-config'

# Generate the new interface

template_filename='interface'

./generate-interface-common.sh \
	"${namespace_camel}" \
	"${interfacename_camel}" \
	"${filename}" \
	"${template_filename}"
