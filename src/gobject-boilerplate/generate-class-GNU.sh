#!/usr/bin/env bash

# configuration

namespace_camel=Gte
classname_camel=Adjustment
filename=gte-adjustment

# generate the new class

template_filename=class-GNU

./generate-class-common.sh \
	${namespace_camel} \
	${classname_camel} \
	${filename} \
	${template_filename}
