gcu-case-converter
==================

Converts a word to `lower_case`, `UPPER_CASE` or `CamelCase`.

Read the top of `gcu-case-converter.c` for more details.
