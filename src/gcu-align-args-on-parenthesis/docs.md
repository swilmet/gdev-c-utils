gcu-align-args-on-parenthesis
=============================

Align function arguments on the opening parenthesis, made to be integrated in a
text editor.

Example:

```
function_call (argument1,
               argument2,
               argument3);
```

Read the top of `gcu-align-args-on-parenthesis.c` for more details.
