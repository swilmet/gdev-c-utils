gcu-check-chain-ups
===================

Basic check of GObject virtual function chain-ups.

Read the top of `gcu-check-chain-ups.c` for more details.
