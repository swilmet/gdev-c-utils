gcu-multi-line-substitution
===========================

Does a multi-line substitution. Or, in other words, a multi-line search and
replace.

Read the top of `gcu-multi-line-substitution.c` for more details.
