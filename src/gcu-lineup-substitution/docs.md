gcu-lineup-substitution
=======================

Do a substitution and at the same time keep a good alignment of arguments on
the parenthesis.

Example:

```
function_call (param1,
               param2,
               param3);
```

Rename `function_call` to `another_beautiful_name`:

```
another_beautiful_name (param1,
                        param2,
                        param3);
```

gcu-lineup-substitution can be useful to rename a GObject class, or to change
the namespace of a group of GObjects, while still keeping a good
indentation/alignment of the code (in combination with gcu-lineup-parameters).

Read the top of `gcu-lineup-substitution.c` for more details.
