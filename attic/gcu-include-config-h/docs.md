gcu-include-config-h
====================

Ensures that `config.h` is `#included` in `*.c` files.

Read the top of `gcu-include-config-h.c` for more details.
