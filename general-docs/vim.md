Vim integration of some gdev-c-utils programs
=============================================

See:
- https://gitlab.gnome.org/swilmet/vim-easy-c
- https://gitlab.gnome.org/swilmet/vim-swilmet-config
