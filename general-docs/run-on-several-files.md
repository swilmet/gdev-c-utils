Running a gdev-c-utils program on several files at once
=======================================================

It is recommended to use e.g.
[GNU Parallel](https://www.gnu.org/software/parallel/) to run a utility on
several files at once. In a previous version of gdev-c-utils it was possible to
directly pass several file arguments to some gcu programs, but this possibility
has been removed to simplify the code, and with GNU Parallel it is anyway
almost as convenient to use, with the benefit that it runs in parallel.

For example:
```
$ find . -name '*.c' | parallel gcu-lineup-parameters
```
