gdev-c-utils
============

Various utilities, useful when programming with
[GLib/GTK](https://www.gtk.org/) in C.

Some utilities are more general and can be useful for other tasks.

Dependencies
------------

Required:
- glib-2.0

Optional:
- gio-unix-2.0
- libgedit-tepl-6

If the optional dependencies are not found, the programs depending on them
won't be built.
